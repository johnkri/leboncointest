//
//  LeBonCoinTestTests.swift
//  LeBonCoinTestTests
//
//  Created by John KRICORIAN on 09/09/2019.
//  Copyright © 2019 John KRICORIAN. All rights reserved.
//

import XCTest
@testable import LeBonCoinTest

class LeBonCoinTestTests: XCTestCase {
    
    func testToCelcius() {
        let value = 3.3
        let stringValue = value.toCelsius()
        XCTAssertNotNil(stringValue)
    }
    
    func testCityNamePersistance() {
        let cityName = "testCity"
        Persistence.saveCityNameToUserDefault(cityName: cityName)
        let peristedCityName = Persistence.loadCityNameFromUserDefaults()
        XCTAssertEqual(peristedCityName, cityName)
    }
    
    func testDatesPersistance() {
        let dates = ["0000", "0001", "0002"]
        Persistence.saveDatesToUserDefault(dates: dates)
        let peristedDates = Persistence.loadDatesFromUserDefaults()
        XCTAssertEqual(peristedDates, dates)
    }
    
    func testRouded() {
        let value = 0.123456789
        let roundedValue = value.rounded(toPlaces: 4)
        XCTAssertEqual(roundedValue, 0.1235)
    }
    
    func testDate() {
        let value = "2019-09-11 05:00:00"
        let timeStamp = value.toTimeStamp()
        XCTAssertEqual(timeStamp, 1568170800)
    }
    
    func testDateViewModel() {
        let dates = ["0000", "0001", "0002"]
        let dateVM = DateListViewModel(dates: dates)
        XCTAssertNotNil(dateVM)
    }
    
}
