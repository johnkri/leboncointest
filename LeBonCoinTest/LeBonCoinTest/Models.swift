//
//  Models.swift
//  LeBonCoin
//
//  Created by John Kricorian on 08/09/2019.
//  Copyright © 2019 John Kricorian. All rights reserved.
//

import Foundation


struct Forcast {
    var date: String
    var sol: Double
    var humidite: Double
    var vent: Double
    
    init(date: String, sol: Double, humidite: Double, vent: Double) {
        self.date = date 
        self.sol = sol
        self.humidite = humidite
        self.vent = vent
    }
}
