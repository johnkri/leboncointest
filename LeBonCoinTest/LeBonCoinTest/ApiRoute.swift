//
//  ApiRoute.swift
//  LeBonCoin
//
//  Created by John Kricorian on 08/09/2019.
//  Copyright © 2019 John Kricorian. All rights reserved.
//

import Foundation
import CoreLocation

class ApiRoute {
    
    static func urlWithCordinate(locValue: CLLocationCoordinate2D?) -> URL? {
        
        let latitude = locValue?.latitude.rounded(toPlaces: 5)
        let longitude = locValue?.longitude.rounded(toPlaces: 5)
        
        let url = URL(string: "https://www.infoclimat.fr/public-api/gfs/json?_ll=\(latitude!),\(longitude!)&_auth=Bx1TRAB%2BByUDLlJlA3UAKQNrV2JZLwgvAn4BYlgzVCkIbABgUjEDYVM4USwDLAEoUWMBfwA5AjpWPQdjWjNWKgd7UzQAYwdmA2RSMwM3ADUDL1coWXsIMQJ%2BAXlYMlQ%2BCHUAbVI0A39TPVE7AzoBK1FkAWIAIAIlVjQHZVoxVjIHbVMzAGIHbANoUjEDLAArAzVXY1lhCDQCNAFjWDZUMghrAGFSYAM1UzRRMAMtATRRagFiAD0CPlY8B2haPlYqB3tTTgAQB3gDLFJyA2YAcgMtV2JZOAhk&_c=d2d4229882c7394ff4e1caeafeb812f0")
        
        return url
    }
}



