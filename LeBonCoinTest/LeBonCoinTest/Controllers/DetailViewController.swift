//
//  DetailViewController.swift
//  LeBonCoinTest
//
//  Created by John KRICORIAN on 09/09/2019.
//  Copyright © 2019 John KRICORIAN. All rights reserved.
//

import UIKit
import RxSwift

class DetailViewController: UIViewController {
    
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var tempsolLabel: UILabel!
    @IBOutlet weak var humiditeLabel: UILabel!
    @IBOutlet weak var ventLabel: UILabel!
    @IBOutlet weak var pluieLabel: UILabel!
    @IBOutlet weak var nebulositeLabel: UILabel!
    
    var date: String?
    var forcast: Forecast?
    let disposeBag = DisposeBag()
    
    func populateViewModels() {
        guard let forcast = forcast, let date = date else { return }
        
        let forecastViewModel = ForecastViewModel(forecast: forcast)
        let dateViewModel = DateViewModel(date: date)
        
        dateViewModel.forcastDate.asDriver(onErrorJustReturn: "")
            .drive(dateLabel.rx.text)
            .disposed(by: disposeBag)
        
        forecastViewModel.temperature.asDriver(onErrorJustReturn: "")
            .drive(tempsolLabel.rx.text)
            .disposed(by: disposeBag)
        
        
        forecastViewModel.nebulosite.asDriver(onErrorJustReturn: "")
            .drive(nebulositeLabel.rx.text)
            .disposed(by: disposeBag)
        
        forecastViewModel.pluie.asDriver(onErrorJustReturn: "")
            .drive(pluieLabel.rx.text)
            .disposed(by: disposeBag)
        
        forecastViewModel.humidite.asDriver(onErrorJustReturn: "")
            .drive(humiditeLabel.rx.text)
            .disposed(by: disposeBag)
        
        forecastViewModel.vent.asDriver(onErrorJustReturn: "")
            .drive(ventLabel.rx.text)
            .disposed(by: disposeBag)
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationController?.navigationBar.prefersLargeTitles = true
        populateViewModels()
    }
}
