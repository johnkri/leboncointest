//
//  ViewController.swift
//  LeBonCoin
//
//  Created by John Kricorian on 08/09/2019.
//  Copyright © 2019 John Kricorian. All rights reserved.
//

import UIKit
import CoreLocation
import MapKit
import RxSwift

class ViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var cityName: UILabel!
    
    private var locationManager = CLLocationManager()
    private var startLocation: CLLocation!
    
    private var dates = [String]()
    var forecasts = [Forecast]()

    let disposeBag = DisposeBag()
    
    private var forecastListViewModel: ForecastListViewModel!
    private var dateListViewModel: DateListViewModel!
    

    func convertLatLongToAddress(latitude: Double, longitude: Double){
        
        let geoCoder = CLGeocoder()
        let location = CLLocation(latitude: latitude, longitude: longitude)
        geoCoder.reverseGeocodeLocation(location, completionHandler: { (placemarks, error) -> Void in
            
            guard let placemarks = placemarks else { return }
            
            var placeMark: CLPlacemark!
            placeMark = placemarks[0]
            
            if let city = placeMark.subAdministrativeArea {
                self.cityName.text = city
                Persistence.saveCityNameToUserDefault(cityName: city)
            }
        })
    }
    
    private func populateNewsWith(url: URL) {
        let resource = Resource<ForecastResponse>(url:url)
        
        URLRequest.load(resource: resource)
            .subscribe(onNext: { forecastResponse in

                var forecastDict = forecastResponse.forecastDict.sorted(by: { $0.0 < $1.0 })
                forecastDict = [forecastDict[0]] + forecastDict
                    .enumerated()
                    .filter({($0.offset + 1) % 9 == 0})
                    .map({$0.element})
                for (date, forcast) in forecastDict {
                    self.dates.append(date)
                    self.forecasts.append(forcast)
                }
                
                self.forecastListViewModel = ForecastListViewModel(forecasts: self.forecasts)
                self.dateListViewModel = DateListViewModel(dates: self.dates)
                Persistence.saveForecastsToUserDefault(forecastResponse: self.forecasts)
                Persistence.saveDatesToUserDefault(dates: self.dates)
                DispatchQueue.main.async {
                    self.tableView.reloadData()
                }
            }).disposed(by: disposeBag)
    }
    
    func updateDeviceLocation() {
        locationManager = CLLocationManager()
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.requestAlwaysAuthorization()
        
        if CLLocationManager.locationServicesEnabled(){
            locationManager.startUpdatingLocation()
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
    
        self.navigationController?.navigationBar.prefersLargeTitles = true
        updateDeviceLocation()
        
        if forecasts.isEmpty && dates.isEmpty {
            self.forecastListViewModel = ForecastListViewModel(forecasts: Persistence.loadForecastsFromUserDefaults())
            self.dateListViewModel = DateListViewModel(dates: Persistence.loadDatesFromUserDefaults())
            self.cityName.text = Persistence.loadCityNameFromUserDefaults()
        }
    }
}

extension ViewController: UITableViewDataSource {
    
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.forecastListViewModel == nil ? 0 : forecastListViewModel.forecastViewModels.count
    }
    
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "PeriodTableViewCell", for: indexPath) as? PeriodTableViewCell else {
            fatalError("PeriodTableViewCell is not found")
        }
        
        let periodViewModel = forecastListViewModel.periodAt(indexPath.row)
        let dateViewModel = dateListViewModel.dateAt(indexPath.row)
        cell.dateLabel.text = String(indexPath.row)

        dateViewModel.forcastDate.asDriver(onErrorJustReturn: "")
            .drive(cell.dateLabel.rx.text)
            .disposed(by: disposeBag)

        periodViewModel.temperature.asDriver(onErrorJustReturn: "")
            .drive(cell.tempLabel.rx.text)
            .disposed(by: disposeBag)
        
        return cell
    }
}

extension ViewController: UITableViewDelegate {
    
    public func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard let destinationVC = storyboard?.instantiateViewController(withIdentifier: "DetailViewController") as? DetailViewController else { return }
        
        if !forecasts.isEmpty && !dates.isEmpty {
            destinationVC.forcast = forecasts[indexPath.row]
            destinationVC.date = dates[indexPath.row]
        } else {
            destinationVC.forcast = Persistence.loadForecastsFromUserDefaults()[indexPath.row]
            destinationVC.date = Persistence.loadDatesFromUserDefaults()[indexPath.row]
        }

        self.navigationController?.pushViewController(destinationVC, animated: true)
    }
}

extension ViewController: CLLocationManagerDelegate {
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        guard let locValue: CLLocationCoordinate2D = manager.location?.coordinate else { return }

        convertLatLongToAddress(latitude: locValue.latitude, longitude: locValue.longitude)
        let userLocation:AnyObject = locations[0]
        
        if startLocation == nil {
            startLocation = userLocation as? CLLocation
            locationManager.stopUpdatingLocation()
            populateNewsWith(url: ApiRoute.urlWithCordinate(locValue: locValue)!)
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print("Error \(error)")
    }
}

