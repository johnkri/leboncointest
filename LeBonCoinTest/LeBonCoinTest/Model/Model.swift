//
//  Models.swift
//  LeBonCoin
//
//  Created by John Kricorian on 08/09/2019.
//  Copyright © 2019 John Kricorian. All rights reserved.
//

import Foundation

public struct Forecast: Codable {
    public let temperature: Temperature
    public let pression: Pression
    public let pluie: Double
    public let nebulosite: Nebulosite
    public let humidite: Humidite
    public let vent_moyen: Vent
}

public struct Vent: Codable {
    public let tenM: Double
    
    enum CodingKeys: String, CodingKey {
        case tenM = "10m"
    }
}

public struct Humidite: Codable {
    public let twoM: Double
    
    enum CodingKeys: String, CodingKey {
        case twoM = "2m"
    }
}

public struct Pression: Codable {
    public let niveau_de_la_mer: Int
}

public struct Temperature: Codable {
    public let sol: Double
}

public struct Nebulosite: Codable {
    public let basse: Double
}

public struct ForecastResponse: Codable {
    
    public var requestState = 0
    public var request_key = ""
    public var message = ""
    public var model_run = ""
    public var source = ""
    public var forecastDict = [String: Forecast]()
    
    private struct CustomCodingKeys: CodingKey {
        var stringValue: String
        init?(stringValue: String) { self.stringValue = stringValue }
        var intValue: Int?
        init?(intValue: Int) { return nil }
    }
    
    public init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CustomCodingKeys.self)
        
        for key in container.allKeys {
            if key.stringValue == "request_state" {
                requestState = try container.decode(Int.self, forKey: CustomCodingKeys(stringValue: key.stringValue)!)
            } else if key.stringValue == "request_key" {
                request_key = try container.decode(String.self, forKey: CustomCodingKeys(stringValue: key.stringValue)!)
            } else if key.stringValue == "message" {
                message = try container.decode(String.self, forKey: CustomCodingKeys(stringValue: key.stringValue)!)
            } else if key.stringValue == "model_run" {
                model_run = try container.decode(String.self, forKey: CustomCodingKeys(stringValue: key.stringValue)!)
            } else if key.stringValue == "source" {
                source = try container.decode(String.self, forKey: CustomCodingKeys(stringValue: key.stringValue)!)
            } else {
                let value = try container.decode(Forecast.self, forKey: CustomCodingKeys(stringValue: key.stringValue)!)
                forecastDict[key.stringValue] = value
            }
        }
    }
}



