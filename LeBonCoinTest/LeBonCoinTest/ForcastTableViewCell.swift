//
//  ForcastTableViewCell.swift
//  LeBonCoin
//
//  Created by John Kricorian on 08/09/2019.
//  Copyright © 2019 John Kricorian. All rights reserved.
//

import UIKit

class ForcastTableViewCell: UITableViewCell {

    @IBOutlet weak var dateLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
