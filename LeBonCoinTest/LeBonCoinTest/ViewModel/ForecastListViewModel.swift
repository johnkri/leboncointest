//
//  ForecastListViewModel.swift
//  LeBonCoinTest
//
//  Created by John KRICORIAN on 09/09/2019.
//  Copyright © 2019 John KRICORIAN. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa

struct ForecastListViewModel {
    let forecastViewModels: [ForecastViewModel]
}

extension ForecastListViewModel {
    
    init(forecasts: [Forecast]) {
        self.forecastViewModels = forecasts.compactMap(ForecastViewModel.init)
    }
}

extension ForecastListViewModel {
    
    func periodAt(_ index: Int) -> ForecastViewModel {
        return self.forecastViewModels[index]
    }
}

struct ForecastViewModel {
    
    let forecast: Forecast
    
    init(forecast: Forecast) {
        self.forecast = forecast
    }
    
}

extension ForecastViewModel {
    
    var temperature: Observable<String> {
        let temp = "\(forecast.temperature.sol.toCelsius().toString()) °"
        return Observable<String>.just(temp)
    }
    
    var pluie: Observable<String> {
        let pluie = "\(forecast.pluie.toString()) mm"
        return Observable<String>.just(pluie)
    }
    
    var nebulosite: Observable<String> {
        let nebulosite = forecast.nebulosite.basse.toString()
        return Observable<String>.just(nebulosite)
    }
    
    var humidite: Observable<String> {
        let humidite = "\(forecast.humidite.twoM.toString()) %"
        return Observable<String>.just(humidite)
    }
    
    var vent: Observable<String> {
        let vent = "\(forecast.vent_moyen.tenM.toString()) km/h"
        return Observable<String>.just(vent)
    }
    
}


