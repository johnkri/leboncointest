//
//  DateViewModel.swift
//  LeBonCoinTest
//
//  Created by John KRICORIAN on 11/09/2019.
//  Copyright © 2019 John KRICORIAN. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa

struct DateListViewModel {
    let dateViewModels: [DateViewModel]
}

extension DateListViewModel {
    
    init(dates: [String]) {
        self.dateViewModels = dates.compactMap(DateViewModel.init)
    }
}

extension DateListViewModel {
    
    func dateAt(_ index: Int) -> DateViewModel {
        return self.dateViewModels[index]
    }
}

struct DateViewModel {
    
    let date: String
    
    init(date: String) {
        self.date = date
    }
    
}

extension DateViewModel {
    
    var forcastDate: Observable<String> {
        return Observable<String>.just(date.toTimeStamp().toStringDate())
    }
    
}


