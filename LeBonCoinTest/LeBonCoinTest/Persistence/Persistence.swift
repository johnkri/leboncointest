//
//  Persistence.swift
//  LeBonCoinTest
//
//  Created by John KRICORIAN on 06/09/2019.
//  Copyright © 2019 John KRICORIAN. All rights reserved.
//

import Foundation

class Persistence {
    
    static func saveForecastsToUserDefault(forecastResponse: [Forecast]) {
        let defaults = UserDefaults.standard
        defaults.set(try? PropertyListEncoder().encode(forecastResponse), forKey: "forecasts")
    }
    
    static func loadForecastsFromUserDefaults() -> [Forecast] {
        let defaults = UserDefaults.standard
        
        guard let playerData = defaults.object(forKey: "forecasts") as? Data else {
            return [Forecast]()
        }
        
        guard let forcasts = try? PropertyListDecoder().decode([Forecast].self, from: playerData) else {
            return [Forecast]()
        }

     return forcasts
    }
    
    static func saveDatesToUserDefault(dates: [String]) {
        let defaults = UserDefaults.standard
        defaults.set(try? PropertyListEncoder().encode(dates), forKey: "dates")
    }
    
    static func loadDatesFromUserDefaults() -> [String] {
        let defaults = UserDefaults.standard
        
        guard let datesData = defaults.object(forKey: "dates") as? Data else {
            return [String]()
        }
        
        guard let dates = try? PropertyListDecoder().decode([String].self, from: datesData) else {
            return [String]()
        }
        
        return dates
    }
    
    static func saveCityNameToUserDefault(cityName: String) {
        UserDefaults.standard.set(cityName, forKey: "cityName")
    }
    
    static func loadCityNameFromUserDefaults() -> String {
        guard let myLoadedString = UserDefaults.standard.string(forKey: "cityName") else {
            return ""
        }
        return myLoadedString
    }
}



