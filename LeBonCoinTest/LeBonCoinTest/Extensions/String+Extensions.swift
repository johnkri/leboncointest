//
//  String+Extensions.swift
//  LeBonCoinTest
//
//  Created by John KRICORIAN on 09/09/2019.
//  Copyright © 2019 John KRICORIAN. All rights reserved.
//

import Foundation

extension String {
    
     func toTimeStamp() -> Int {
        let myDate = self
        let dateFormat = DateFormatter()
        dateFormat.dateFormat = "yyyy-MM-dd HH:mm:ss"
        let date = dateFormat.date(from: myDate)
        let dateStamp:TimeInterval = date!.timeIntervalSince1970
        let dateSt = Int(dateStamp)
        
        return dateSt
    }
}
