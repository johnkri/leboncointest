//
//  Int+Extension.swift
//  LeBonCoin
//
//  Created by John Kricorian on 08/09/2019.
//  Copyright © 2019 John Kricorian. All rights reserved.
//

import Foundation

extension Int {
    
    func toStringDate() -> String {
        let date = Date(timeIntervalSince1970: TimeInterval(self))
        let dateFormatter = DateFormatter()
        dateFormatter.locale = Locale(identifier: "FR-fr")
        dateFormatter.dateFormat = "E d MMM"
        let strDate = dateFormatter.string(from: date)
        
        return strDate
    }
    
    func toString() -> String {
        let myString = String(self)
        return myString
    }
}
