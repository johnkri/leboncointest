//
// Double+Extension.Swift
// LeBonCoinTest
//
//  Created by John KRICORIAN on 06/09/2019.
//  Copyright © 2019 John KRICORIAN. All rights reserved.
//

import Foundation

extension Double {
    func toString() -> String {
        return String(format: "%.1f",self)
    }
    
    func toCelsius() -> Int {
        return Int(self - 273.15)
    }
    
    func rounded(toPlaces places:Int) -> Double {
        let divisor = pow(10.0, Double(places))
        return (self * divisor).rounded() / divisor
    }
}
